#ifndef CLIENT_H
#define CLIENT_H
#include <string>

using namespace std;

class client
{
    public:
        client(int id, string name);
        ~client();
        string name;
        int GetId();
    private:
        int id;
};

#endif // CLIENT_H
