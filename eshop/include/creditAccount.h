#ifndef CREDITACCOUNT_H
#define CREDITACCOUNT_H

#include "account.h"



class creditAccount : public account
{
    public:
        creditAccount(int code, client *o);
        ~creditAccount();
        void SetCredit(int c);
        int GetCredit();
        bool CanPay(produkt *p);
        bool Pay(produkt *p);

    protected:
        int credit;
    private:
};

#endif // CREDITACCOUNT_H
