#ifndef PRODUKT_H
#define PRODUKT_H
#include <string>

using namespace std;

class produkt
{
    public:
        produkt(int prize, string label);
        ~produkt();
        int prize;
        string label;
    private:
};

#endif // PRODUKT_H
