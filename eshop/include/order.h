#ifndef ORDER_H
#define ORDER_H
#include <string>
#include "account.h"
#include "produkt.h"

class order
{
    public:
        order(account *acc, produkt *prod);
        ~order();
        account *GetAcc();
        produkt *GetProd();
    private:
        account *acc;
        produkt *prod;
};

#endif // ORDER_H
