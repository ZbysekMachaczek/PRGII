#ifndef ESHOP_H
#define ESHOP_H
#include <list>
#include <string>
#include <queue>
#include "account.h"
#include "order.h"

using namespace std;

class eshop
{
    public:
        eshop();
        ~eshop();

                                                /**Creating objects**/
        client *CreateClient(int id, string name);
        account *CreateAccount(int code, client *o);
        produkt *CreateProdukt(int prize, string label);

                                /**Finding particular objects given characteristics**/
        client *GetClient(int id);
        account *GetAccount(int code);
        produkt *GetProdukt(string label);

                                                /**Ordering**/
        order *MakeOrder(account *acc, produkt *prod);
        order *SolveOrder();

        int GetNumOfAcc();

    private:
                            /**Database**/
        list<produkt*> products;
        list<account*> accounts;
        list<client*> clients;

        queue<order*> Q;
};

#endif // ESHOP_H
