#ifndef ACCOUNT_H
#define ACCOUNT_H
#include "client.h"
#include "produkt.h"

class account
{
    public:
        account(int code, client *o);
        ~account();
        int GetCode();
        client *GetOwner();
        static int GetAccCount();

    protected:
        int credit;

    private:
        static int accCount;
        int code;
        client *owner;
};

#endif // ACCOUNT_H
