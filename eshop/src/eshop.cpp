#include "eshop.h"

using namespace std;

eshop::eshop()
{

}

eshop::~eshop()
{
    //dtor
}

client *eshop::CreateClient(int id, string name)
{
    client *nC = new client(id, name);
    this->clients.push_back(nC);

    return nC;
}

account *eshop::CreateAccount(int code, client *o)
{
    account *nA = new account(code, o);
    this->accounts.push_back(nA);

    return nA;
}

produkt *eshop::CreateProdukt(int prize, string label)
{
    produkt *nP = new produkt(prize, label);
    this->products.push_back(nP);

    return nP;
}

client *eshop::GetClient(int id)
{
    for(client *i : this->clients)
    {
        if(i->GetId() == id)
        {
            return i;
        }
    }
    return nullptr;
}



account *eshop::GetAccount(int code)
{
    for(account *i : this->accounts)
    {
        if(i->GetCode() == code)
        {
            return i;
        }
    }
    return nullptr;
}

produkt *eshop::GetProdukt(string name)
{
    for(produkt *i : this->products)
    {
        if(i->label == name)
        {
            return i;
        }
    }
    return nullptr;
}

order *eshop::MakeOrder(account *acc, produkt *prod)
{
    order *o = new order(acc, prod);
    this->Q.push(o);

    return o;
}

order *eshop::SolveOrder()
{
    order *o = Q.back();
    this->Q.pop();

    return o;
}

int eshop::GetNumOfAcc()
{
    return accounts.back()->GetAccCount();
}











