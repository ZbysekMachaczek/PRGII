#include "creditAccount.h"

using namespace std;

creditAccount::creditAccount(int code, client *o) : account(code, o)
{
}

creditAccount::~creditAccount()
{
    //dtor
}

void creditAccount::SetCredit(int c)
{
    this->credit = c;
}

int creditAccount::GetCredit()
{
    return this->credit;
}

bool creditAccount::CanPay(produkt *p)
{
    if(p->prize <= this->credit)
    {
        return true;
    }

    else
    {
        return false;
    }
}

bool creditAccount::Pay(produkt *p)
{
    if(p->prize <= this->credit)
    {
        this->credit -= p->prize;
        return true;
    }

    else
    {
        return false;
    }
}








