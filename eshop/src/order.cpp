#include "order.h"

order::order(account *acc, produkt *prod)
{
    this->acc = acc;
    this->prod = prod;
}

order::~order()
{
    //dtor
}

account *order::GetAcc()
{
    return this->acc;
}

produkt *order::GetProd()
{
    return this->prod;
}
