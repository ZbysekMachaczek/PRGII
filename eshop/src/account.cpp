#include "account.h"

using namespace std;

int account::accCount = 0;

account::account(int code, client *o)
{
    this->code = code;
    this->owner = o;
    account::accCount++;
}

account::~account()
{
    account::accCount--;
}

int account::GetCode()
{
    return this->code;
}

client *account::GetOwner()
{
    return this->owner;
}

int account::GetAccCount()
{
    return account::accCount;
}
