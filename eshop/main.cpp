#include <iostream>
#include "eshop.h"

using namespace std;

int main()
{
    eshop *E = new eshop();
    E->CreateClient(1,"Jan");
    E->CreateClient(2,"Petr");
    E->CreateAccount(1, E->GetClient(1));
    E->CreateAccount(2, E->GetClient(2));
    E->CreateProdukt(1000, "Boty");
    E->MakeOrder(E->GetAccount(1), E->GetProdukt("Boty"));

    cout << E->GetAccount(1)->GetOwner()->name << endl;

    order *o = E->SolveOrder();
    cout << o->GetProd()->label << endl;

    cout << "Static AccCount: " << E->GetNumOfAcc() << endl;


    return 0;
}
