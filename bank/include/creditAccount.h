#ifndef CREDITACCOUNT_H
#define CREDITACCOUNT_H
#include "account.h"

class creditAccount : public account
{
    public:
        creditAccount(int n, client *c, double cr);
        creditAccount(int n, client *c, double ir, double cr);
        virtual ~creditAccount();
        virtual bool CanWithdraw(double a);

    protected:

    private:
        double credit;
};

#endif // CREDITACCOUNT_H
