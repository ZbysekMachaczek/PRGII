#ifndef BANK_H
#define BANK_H
#include "abstractAccount.h"
#include "partnerAccount.h"

using namespace std;

class bank
{
    private:
        client **clients;
        int clientsCount;

        account **accounts;
        int accountsCount;
    public:
        bank(int c, int a);
        ~bank();

        client *GetClient(int c);
        account *GetAccount(int n);

        client *CreateClient(int c, string n);
        account *CreateAccount(int n, client *c);
        account *CreateAccount(int n, client *c, double ir);
        partnerAccount *CreateAccount(int n, client *c, client *p);
        partnerAccount *CreateAccount(int n, client *c, client *p, double ir);

        int GetAccountNum();

        void AddInterest(int n);
};

#endif // BANK_H
