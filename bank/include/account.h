#ifndef ACCOUNT_H
#define ACCOUNT_H
#include "client.h"
#include "abstractAccount.h"
#include <string>

using namespace std;

class account : public abstractAccount
{
    private:
        static int accCount;
        int number;
        double interestRate;

        client *owner;
        //client *partner;
    public:
        account();
        account(int n, client *c);
        account(int n, client *c, double ir);
        //account(int n, client *c, client *p);
        //account(int n, client *c, client *p, double ir);
        virtual ~account();

        static int GetAccCount();

        int GetNumber();
        double GetBalance();
        double GetInterestRate();
        client *GetOwner();
        //client *GetPartner();
        virtual bool CanWithdraw(double a);

        void Deposit(double a);
        bool Withdraw(double a);
        void AddInterest();

    protected:
        double balance;
};

#endif // ACCOUNT_H
