#ifndef CLIENT_H
#define CLIENT_H
#include <string>

using namespace std;

class client
{
    private:
        int code;
        string name;
    public:
        client(int c, string n);
        ~client();

        int GetCode();
        string GetName();
};

#endif // CLIENT_H
