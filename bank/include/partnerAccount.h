#ifndef PARTNERACCOUNT_H
#define PARTNERACCOUNT_H
#include "client.h"
#include "account.h"

class partnerAccount : public account
{
    public:
        partnerAccount(int n, client *o, client *p);
        partnerAccount(int n, client *o, client *p, double ir);
        virtual ~partnerAccount();

        client *GetPartner();
    private:
        client *partner;
};

#endif // PARTNERACCOUNT_H
