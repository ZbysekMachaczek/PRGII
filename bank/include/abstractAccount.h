#ifndef ABSTRACTACCOUNT_H
#define ABSTRACTACCOUNT_H
#include <iostream>

using namespace std;

class abstractAccount
{
    public:
        abstractAccount();
        virtual ~abstractAccount();
        virtual bool canWithdraw(double a);

    protected:

    private:
};

#endif // ABSTRACTACCOUNT_H
