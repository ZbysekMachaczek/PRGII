#include "partnerAccount.h"

partnerAccount::partnerAccount(int n, client *o, client *p) : account(n, o)
{
    this->partner = p;
}

partnerAccount::partnerAccount(int n, client *o, client *p, double ir) : account(n, o, ir)
{
    this->partner = p;
}

partnerAccount::~partnerAccount()
{

}

client *partnerAccount:: GetPartner()
{
    return this->partner;
}
