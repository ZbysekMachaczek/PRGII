#include "client.h"
#include <string>

using namespace std;

client::client(int c, string n)
{
    this->code = c;
    this->name = n;
}

client::~client()
{
    //dtor
}

int client::GetCode()
{
    return this->code;
}

string client::GetName()
{
    return this->name;
}
