#include "account.h"
#include "client.h"
#include <string>

int account::accCount = 0;

account::account(int n, client *c)
{
    this->number = n;
    this->owner = c;
    account::accCount += 1;
}

account::account(int n, client *c, double ir)
{
    this->number = n;
    this->owner = c;
    this->interestRate = ir;
    account::accCount += 1;
}

account::~account()
{
    account::accCount -= 1;
}

int account::GetAccCount()
{
    return account::accCount;
}

int account::GetNumber()
{
    return this->number;
}

double account::GetBalance()
{
    return this->balance;
}

client *account::GetOwner()
{
    return this->owner;
}


bool account::CanWithdraw(double a)
{
    if(GetBalance() > a)
        return true;
    else
        return false;
}

void account::Deposit(double a)
{
    this->balance += a;
}

bool account::Withdraw(double a)
{
    bool success = false;
    if(GetBalance() > a)
    {
        this->balance -= a;
        success = true;
    }
        return success;
}

void account::AddInterest()
{
    this->interestRate++;
}













