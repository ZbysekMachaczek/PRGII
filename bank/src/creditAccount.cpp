#include "creditAccount.h"

creditAccount::creditAccount(int n, client *c, double cr) : account(n, c)
{
    this->credit = cr;
}

creditAccount::creditAccount(int n, client *c, double ir, double cr) : account(n, c, ir)
{
    this->credit = cr;
}

creditAccount::~creditAccount()
{
    //dtor
}

bool creditAccount::CanWithdraw(double a)
{
    return (this->balance + this->credit) >= a;
}
