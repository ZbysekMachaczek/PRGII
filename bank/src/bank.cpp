#include "bank.h"
#include "client.h"
#include "account.h"
#include "partnerAccount.h"
#include <string>

using namespace std;

bank::bank(int c, int a)
{
    this->clientsCount = c;
    this->accountsCount = a;

    this->clients = new client *[c];
    for(int i = 0; i < this->clientsCount; i++)
    {
        clients[i] = nullptr;
    }

    this->accounts = new account *[a];
    for(int j = 0; j < this->accountsCount; j++)
    {
        accounts[j] = nullptr;
    }
}

bank::~bank()
{
    delete[] this->clients;
    delete[] this->accounts;
}

client *bank::GetClient(int c)
{
    for(int i = 0; i < this->clientsCount; i++)
    {
        if(c == this->clients[i]->GetCode())
        {
            return this->clients[i];
        }
    }
    return nullptr;
}

account *bank::GetAccount(int n)
{
    for(int i = 0; i < this->accountsCount; i++)
    {
        if(n == this->accounts[i]->GetNumber())
        {
            return this->accounts[i];
        }
    }
    return nullptr;
}

client *bank::CreateClient(int c, string n)
{
    for(int i = 0; i < this->clientsCount; i++)
    {
        if(this->clients[i] == nullptr)
        {
            this->clients[i] = new client(c, n);
            return this->clients[i];
        }
    }
    return nullptr;
}

account *bank::CreateAccount(int n, client* c)
{
    for(int i = 0; i < this->accountsCount; i++)
    {
        if(this->accounts[i] == nullptr)
        {
            this->accounts[i] = new account(n, c);
            return this->accounts[i];
        }
    }
    return nullptr;
}

account *bank::CreateAccount(int n, client *c, double ir)
{
    for(int i = 0; i < this->accountsCount; i++)
    {
        if(this->accounts[i] == nullptr)
        {
            this->accounts[i] = new account(n, c, ir);
            return this->accounts[i];
        }
    }
    return nullptr;
}

partnerAccount *bank::CreateAccount(int n, client *c, client *p)
{
    for(int i = 0; i < this->accountsCount; i++)
    {
        if(this->accounts[i] == nullptr)
        {
            partnerAccount *tmp = new partnerAccount(n, c, p); ;
            this->accounts[i] = tmp;

            return tmp;
        }
    }
    return nullptr;
}

partnerAccount *bank::CreateAccount(int n, client *c, client *p, double ir)
{
    for(int i = 0; i < this->accountsCount; i++)
    {
        if(this->accounts[i] == nullptr)
        {
            partnerAccount *tmp = new partnerAccount(n, c, p, ir);
            this->accounts[i] = tmp;

            return tmp;
        }
    }
    return nullptr;
}

void bank::AddInterest(int n)
{
    account *acc = GetAccount(n);
    acc->AddInterest();
}

int bank::GetAccountNum()
{
    return this->accounts[this->accountsCount]->GetAccCount();
}







