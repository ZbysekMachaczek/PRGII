#include <iostream>
#include <string>
#include <vector>
#include "bank.h"

using namespace std;

int main()
{
    bank *Banka = new bank(3, 2);
    Banka->CreateClient(1, "Jan");
    Banka->CreateClient(2, "Eliska");
    Banka->CreateClient(3, "Petr");

    client *cl = new client(10, "Jana");
    account *acc = new account(1, cl);
    vector<abstractAccount*> store;
    store.push_back(acc);

    Banka->CreateAccount(1, Banka->GetClient(1), Banka->GetClient(2), 1.2);
    Banka->CreateAccount(2, Banka->GetClient(3));

    cout << Banka->GetAccount(1)->GetOwner()->GetName() << endl;
    cout << Banka->GetAccountNum() << endl;

    return 0;
}
