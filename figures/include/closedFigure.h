#ifndef CLOSEDFIGURE_H
#define CLOSEDFIGURE_H

#include "figure.h"

using namespace std;

class closedFigure : public figure
{
    public:
        closedFigure(vector<double> parameter);
        virtual ~closedFigure();

    protected:

    private:
};

#endif // CLOSEDFIGURE_H
