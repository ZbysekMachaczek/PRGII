#ifndef SQUARE_H
#define SQUARE_H

#include "rectangle.h"

using namespace std;

class square : public rectangle
{
    public:
        square(vector<double> parameter);
        virtual ~square();
    protected:
    private:
};

#endif // SQUARE_H
