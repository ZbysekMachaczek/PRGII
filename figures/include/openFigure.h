#ifndef OPENFIGURE_H
#define OPENFIGURE_H

#include "figure.h"


class openFigure : public figure
{
    public:
        openFigure(vector<double> parameter);
        virtual ~openFigure();
    protected:
    private:
};

#endif // OPENFIGURE_H
