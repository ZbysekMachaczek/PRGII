#ifndef SEGMENT_H
#define SEGMENT_H

#include "openFigure.h"

using namespace std;

class segment : public openFigure
{
    public:
        segment(vector<double> parameter);
        virtual ~segment();
    protected:
    private:
};

#endif // SEGMENT_H
