#ifndef CIRCLE_H
#define CIRCLE_H

#include "elipse.h"
#include <cmath>
#define _USE_MATH_DEFINES

using namespace std;

class circle : public elipse
{
    public:
        circle(vector<double> parameter);
        virtual ~circle();
        virtual double area();
        virtual double perimeter();

    protected:
    private:
};

#endif // CIRCLE_H
