#ifndef RECTANGLE_H
#define RECTANGLE_H

#include "quadrangle.h"

using namespace std;

class rectangle : public quadrangle
{
    public:
        rectangle(vector<double> parameter);
        virtual ~rectangle();
        virtual double area();
        virtual double perimeter();
    protected:
    private:
};

#endif // RECTANGLE_H
