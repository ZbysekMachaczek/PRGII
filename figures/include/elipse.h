#ifndef ELIPSE_H
#define ELIPSE_H

#include "closedFigure.h"
#include <cmath>

using namespace std;

class elipse : public closedFigure
{
    public:
        elipse(vector<double> parameter);
        virtual ~elipse();
        virtual double area();
        virtual double perimeter();
    protected:
    private:
};

#endif // ELIPSE_H
