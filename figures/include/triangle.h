#ifndef TRIANGLE_H
#define TRIANGLE_H

#include "polygon.h"

using namespace std;

class triangle : public polygon
{
    public:
        triangle(vector<double> parameter);
        virtual ~triangle();
        virtual double area();
        virtual double perimeter();
    protected:
    private:
};

#endif // TRIANGLE_H
