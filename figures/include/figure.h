#ifndef FIGURE_H
#define FIGURE_H
#include <vector>
#include <iostream>

using namespace std;

class figure
{
    public:
        figure(vector<double> parameter);
        virtual ~figure();
        virtual double area();
        virtual double perimeter();

    protected:
        vector<double> parameter;

    private:
};

#endif // FIGURE_H
