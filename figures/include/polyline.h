#ifndef POLYLINE_H
#define POLYLINE_H

#include "openFigure.h"


class polyline : public openFigure
{
    public:
        polyline(vector<double> parameter);
        virtual ~polyline();
    protected:
    private:
};

#endif // POLYLINE_H
