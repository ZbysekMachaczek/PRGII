#ifndef POLYGON_H
#define POLYGON_H

#include "closedFigure.h"

using namespace std;

class polygon : public closedFigure
{
    public:
        polygon(vector<double> parameter);
        virtual ~polygon();

    protected:

    private:
};

#endif // POLYGON_H
