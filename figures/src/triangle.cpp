#include "triangle.h"

triangle::triangle(vector<double> parameter) : polygon(parameter)
{
    //ctor
}

triangle::~triangle()
{
    //dtor
}

double triangle::area()
{
    return (this->parameter[0] * this->parameter[1]) / 2;
}

double triangle::perimeter()
{
    return this->parameter[1] + this->parameter[2] + this->parameter[3];
}
