#include "circle.h"

circle::circle(vector<double> parameter) : elipse(parameter)
{
    //ctor
}

circle::~circle()
{
    //dtor
}

double circle::area()
{
    return atan(1)*4 * pow(this->parameter[0], 2);
}

double circle::perimeter()
{
    return atan(1)*8 * this->parameter[0];
}
