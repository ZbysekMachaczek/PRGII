#include "elipse.h"

elipse::elipse(vector<double> parameter) : closedFigure(parameter)
{
    //ctor
}

elipse::~elipse()
{
    //dtor
}

double elipse::area()
{
    return atan(1)*4 * this->parameter[0] * this->parameter[1];
}

double elipse::perimeter()
{
    return atan(1)*4 * sqrt(2 * ((pow(this->parameter[0],2)) + pow(this->parameter[1], 2)));
}
