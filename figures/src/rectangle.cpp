#include "rectangle.h"

rectangle::rectangle(vector<double> parameter) : quadrangle(parameter)
{
    //ctor
}

rectangle::~rectangle()
{
    //dtor
}

double rectangle::area()
{
    return this->parameter[0] * this->parameter[1];
}

double rectangle::perimeter()
{
    return 2*this->parameter[0] + 2*this->parameter[1];
}
